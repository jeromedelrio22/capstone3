import { useState, useEffect, useContext } from 'react';
import { Form, Button, Image, InputGroup } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'
import MailOutline from '@mui/icons-material/MailOutline';
import { VpnKey } from '@material-ui/icons';

import login from '../images/login.png';

export default function Login() {

    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    function authenticate(e) {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data.access !== undefined){
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Login successful!"
                });
            } else {
                Swal.fire({
                    title: "Authentication failed",
                    icon: "error",
                    text: "Check your login details and try again!"
                })
            }
        })

        setEmail('');
        setPassword('');
    }


    // Retrieving user details

    const retrieveUserDetails = (token) => {

        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {

        if ((email !== '' && password !== '')) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [email, password]);

    return (
<>
    <div className='text-center'>
        <Image className='mt-3' style={{ width: "200px" }} src={login} fluid alt='logo' />
    </div>


    {user.id !== null
        ? <Navigate to='/products'/>
        :
        <Form onSubmit={(e) => authenticate(e)}>
            <h3>Login</h3>
            <Form.Group className='mb-3' controlId="userEmail">
                <InputGroup>
                    <InputGroup.Text>
                        <MailOutline style={{ color: 'gray' }} />
                    </InputGroup.Text>
                    <Form.Control 
                        type="email"
                        placeholder="Email address"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        className="border-0 border-bottom border-secondary"
                    />
                </InputGroup>
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
                <InputGroup>
                    <InputGroup.Text>
                        <VpnKey style={{ color: 'gray' }} />
                    </InputGroup.Text>
                    <Form.Control 
                        type="password"
                        placeholder="Password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        className="border-0 border-bottom border-secondary"
                    />
                </InputGroup>
            </Form.Group>

            {isActive
                ? <Button variant="primary" type="submit" id="submitBtn" style={{ width: "100%" }}>Login</Button>
                : <Button variant="secondary" type="submit" id="submitBtn" style={{ width: "100%" }} disabled>Login</Button>
            }
        </Form>
    }
</>
    )
}
