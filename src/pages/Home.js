import { Container } from 'react-bootstrap';
import Banner from '../components/Banner';


export default function Home() {

  const data = {
    
    destination: "/products",
    label: "Buy now!"
  }
  return (
    <>
      <Container>
            <Banner data={data}/>          
          </Container>
        </>
  )
}
