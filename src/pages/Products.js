import { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';
import { Col, Row } from 'react-bootstrap';

export default function Products() {

	// State that will be used to store the products retrieved from the database
	const [products, setProducts ] = useState([]);

	// Retrieves the products from the database upon initial render of the "Products" component
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/allProducts`)
		.then(res => res.json())
		.then(data => {
			// Sets the "products" state to map the data retrieved from the fetch request into several "ProductCard" components
			setProducts(data);
		})
	}, [])

	return (
		<>
			<h1>Products</h1>	
			<Row>
				{products.map(product => (
					<Col key={product._id} xs={12} md={6} lg={4}>
						<ProductCard productProp={product} />
					</Col>
				))}
			</Row>
		</>
	)
}
