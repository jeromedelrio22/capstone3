import { useState, useEffect } from "react";
import { Modal, Button, Form } from "react-bootstrap";

const OrdersModal = ({ orders, showOrders, setShowOrders, handleFormSubmit }) => {
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    setIsActive(orders.length > 0);
  }, [orders]);

const computeTotalAmount = (products) => {
  let total = 0;
  products.forEach((product) => {
    total += product.price * product.quantity;
  });
  return total;
};

const computeUserTotalAmount = (orders, userEmail) => {
  const userOrders = orders.filter((order) => order.userEmail === userEmail);
  let userTotalAmount = 0;
  userOrders.forEach((order) => {
    userTotalAmount += computeTotalAmount(order.products);
  });
  return userTotalAmount;
};

const adminEmail = "usop@mail.com";

console.log('Orders:', orders);

const totalAmount = orders
  .filter((order) => order.userEmail !== adminEmail)
  .reduce((acc, order) => {
    const amount = parseFloat(order.totalAmount);
    if (!isNaN(amount)) {
      return acc + amount;
    }
    return acc;
  }, 0);

console.log('Total Amount:', totalAmount);


console.log(`Total amount: ${totalAmount}`);



  return (
    <Modal show={showOrders} fullscreen={true} onHide={() => setShowOrders(false)}>
      <Form onSubmit={handleFormSubmit}>
        <Modal.Header closeButton>
          <Modal.Title>List Of Orders</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {isActive ? (
            <div>
              {orders.map((order) => (
                <div key={order.purchasedOn}>
                  <h2>{order.userEmail}</h2>
                  <p>Total Amount: Php{computeUserTotalAmount(orders, order.userEmail)}</p>
                  <p>Purchased On: {order.purchasedOn}</p>
                  <ul>
                    {order.products.map((product) => (
                      <li key={product.productId}>
                        {product.productName} ({product.quantity}) - Php{product.price}
                      </li>
                    ))}
                  </ul>
                </div>
              ))}
            </div>
          ) : (
            <div>No orders to display</div>
          )}
        </Modal.Body>
        <Modal.Footer>
          {isActive ? (
            <Button variant="primary" type="submit" id="submitBtn">
              Save
            </Button>
          ) : (
            <Button variant="danger" type="submit" id="submitBtn" disabled>
              Save
            </Button>
          )}
          <Button variant="secondary" onClick={() => setShowOrders(false)}>
            Close
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};

export default function AdminDashboard() {
  const [showAdd, setShowAdd] = useState(false);
  const [showEdit, setShowEdit] = useState(false);
  const [orders, setOrders] = useState([]);
  const [showOrders, setShowOrders] = useState(false);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/getOrders`)
      .then((response) => response.json())
      .then((data) => setOrders(data))
      .catch((error) => console.log(error));
  }, []);

  const handleFormSubmit = (e) => {
    e.preventDefault();
    const formData = new FormData(e.target);
    const orderData = {
      name: formData.get("name"),
      email: formData.get("email"),
      // etc.
    };
    fetch(`${process.env.REACT_APP_API_URL}/users/checkOut`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(orderData),
    })
      .then((response) => response.json())
      .then((data) => {
        // handle the successful response here
        console.log(data);
        alert("Order placed successfully!");
        setShowOrders(false);
      })
      .catch((error) => {
        console.log(error);
        alert("Error placing order. Please try again!");
      });
  };

  return (
    <div>
      <Button variant="success" className="
        mx-2" onClick={() => setShowOrders(true)}>
        View Orders
      </Button>
      <OrdersModal orders={orders} showOrders={showOrders} setShowOrders={setShowOrders} handleFormSubmit={handleFormSubmit} />
    </div>
  );
}
